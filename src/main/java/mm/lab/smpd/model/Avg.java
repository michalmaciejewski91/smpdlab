package mm.lab.smpd.model;

public class Avg {

	private double sum;
	private int counter;

	public Avg() {
		sum = 0.0;
		counter = 0;
	}

	public void addParam(double param) {
		sum += param;
		counter++;
	}

	public double getValue() {
		return sum / counter;
	}

	public int getCounter() {
		return counter;
	}
}
