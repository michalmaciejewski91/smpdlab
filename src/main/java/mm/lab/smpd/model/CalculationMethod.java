package mm.lab.smpd.model;

public enum CalculationMethod {
	NN("NN"),
	kNN("k-NN"),
	NM("NM"),
	kNM("k-NM");

	private String displayName;

	CalculationMethod(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}
}
