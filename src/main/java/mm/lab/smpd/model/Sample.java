package mm.lab.smpd.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Sample {

	private String name;
	private List<Double> features;

	private Sample() {
		features = new ArrayList<>();
	}

	public Sample(String name, String[] features) {
		this();
		this.name = name;
		List<String> featuresStringsArray = Arrays.asList(features);
		for (String f : featuresStringsArray) {
			this.features.add(Double.parseDouble(f));
		}
	}

	public String getName() {
		return name;
	}

	public List<Double> getFeatures() {
		return features;
	}
}
