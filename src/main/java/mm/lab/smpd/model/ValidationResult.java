package mm.lab.smpd.model;

public class ValidationResult {

	private String validationType;

	private CalculationResult nnResult;
	private CalculationResult knnResult;
	private CalculationResult nmResult;
	private CalculationResult knmResult;

	public ValidationResult(String validationType) {
		this.validationType = validationType;
		this.nnResult = new CalculationResult(CalculationMethod.NN);
		this.knnResult = new CalculationResult(CalculationMethod.kNN);
		this.nmResult = new CalculationResult(CalculationMethod.NM);
		this.knmResult = new CalculationResult(CalculationMethod.kNM);
	}

	public void setNnResult(CalculationResult nnResult) {
		this.nnResult = nnResult;
	}

	public void setKnnResult(CalculationResult knnResult) {
		this.knnResult = knnResult;
	}

	public void setNmResult(CalculationResult nmResult) {
		this.nmResult = nmResult;
	}

	public void setKnmResult(CalculationResult knmResult) {
		this.knmResult = knmResult;
	}

	@Override
	public String toString() {
		return validationType + " validation result: \n"
			+ nnResult.toString() + "\n"
			+ knnResult.toString() + "\n"
			+ nmResult.toString() + "\n"
			+ knmResult.toString();
	}

	public void addCalculationResult(CalculationResult newCalcResult) {
		CalculationMethod calculationMethod = newCalcResult.getCalculationMethod();
		if (calculationMethod.equals(CalculationMethod.NN)) {
			int ok = nnResult.getOk() + newCalcResult.getOk();
			int ko = nnResult.getKo() + newCalcResult.getKo();
			nnResult = new CalculationResult(nnResult.getCalculationMethod(), ok, ko);
		} else if (calculationMethod.equals(CalculationMethod.kNN)) {
			int ok = knnResult.getOk() + newCalcResult.getOk();
			int ko = knnResult.getKo() + newCalcResult.getKo();
			knnResult = new CalculationResult(knnResult.getCalculationMethod(), ok, ko);
		} else if (calculationMethod.equals(CalculationMethod.NM)) {
			int ok = nmResult.getOk() + newCalcResult.getOk();
			int ko = nmResult.getKo() + newCalcResult.getKo();
			nmResult = new CalculationResult(nmResult.getCalculationMethod(), ok, ko);
		} else {
			int ok = knmResult.getOk() + newCalcResult.getOk();
			int ko = knmResult.getKo() + newCalcResult.getKo();
			knmResult = new CalculationResult(knmResult.getCalculationMethod(), ok, ko);
		}
	}
}
