package mm.lab.smpd.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CalculationResult {

	private CalculationMethod calculationMethod;

	private int ok;
	private int ko;
	private double result;

	public CalculationResult(CalculationMethod calculationMethod) {
		this.calculationMethod = calculationMethod;
		ok = -999999999;
		ko = -999999999;
		result = -999999999;
	}

	public CalculationResult(CalculationMethod calculationMethod, int ok, int ko) {
		this.calculationMethod = calculationMethod;
		this.ok = ok;
		this.ko = ko;

		int all = ok + ko;
		this.result = BigDecimal.valueOf((double) ok / all * 100)
			.setScale(2, RoundingMode.HALF_UP)
			.doubleValue();
	}

	public CalculationMethod getCalculationMethod() {
		return calculationMethod;
	}

	public int getOk() {
		return ok;
	}

	public int getKo() {
		return ko;
	}

	@Override
	public String toString() {
		String info = "[" + calculationMethod.getDisplayName() + " calculation result]";
		if (ok >= 0 && ko >= 0) {
			info += " OK: " + ok + ", KO: " + ko + ", result: " + result + "%";
		} else {
			info += " - not implemented yet, sorry :(";
		}
		return info;
	}
}
