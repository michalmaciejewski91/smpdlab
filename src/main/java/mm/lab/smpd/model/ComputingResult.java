package mm.lab.smpd.model;

import java.util.List;

public class ComputingResult {

	private List<Integer> bestFeatures;
	private double value;

	public ComputingResult(List<Integer> bestFeatures, double value) {
		this.bestFeatures = bestFeatures;
		this.value = value;
	}

	public List<Integer> getBestFeatures() {
		return bestFeatures;
	}

	public double getValue() {
		return value;
	}
}
