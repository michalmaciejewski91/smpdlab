package mm.lab.smpd.model;

public class EuclidDistance implements Comparable<EuclidDistance> {

	private int index;
	private double distance;

	public EuclidDistance(int index, double distance) {
		this.index = index;
		this.distance = distance;
	}

	public int getIndex() {
		return index;
	}

	public int compareTo(EuclidDistance euclidDistance) {
		return Double.compare(this.distance, euclidDistance.distance);
	}
}
