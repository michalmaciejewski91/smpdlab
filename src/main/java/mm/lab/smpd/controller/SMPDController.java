package mm.lab.smpd.controller;

import cern.colt.matrix.DoubleMatrix2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import mm.lab.smpd.model.Avg;
import mm.lab.smpd.model.CalculationMethod;
import mm.lab.smpd.model.CalculationResult;
import mm.lab.smpd.model.ComputingResult;
import mm.lab.smpd.model.Sample;
import mm.lab.smpd.model.ValidationResult;
import org.apache.log4j.Logger;

public class SMPDController {

	private static final Logger LOGGER = Logger.getLogger(SMPDController.class.getName());
	private static final String LINE_PARAM_SEPARATOR;

	static {
		LINE_PARAM_SEPARATOR = ",";
	}

	@FXML
	private AnchorPane mainAnchorPane;

	@FXML
	private ToggleGroup computingToggleGroup;

	@FXML
	private RadioButton fisherRadioButton;

	@FXML
	private RadioButton sfsRadioButton;

	@FXML
	private ToggleGroup validationToggleGroup;

	@FXML
	private RadioButton crossValidationRadioButton;

	@FXML
	private RadioButton bootstrapRadioButton;

	@FXML
	private ComboBox<Integer> numberOfFeaturesComboBox;

	@FXML
	private Label classesLabel;

	@FXML
	private Label samplesLabel;

	@FXML
	private Label featuresLabel;

	@FXML
	private TextField trainingPartTextField;

	@FXML
	private TextField numberOfSetsTextField;

	@FXML
	private TextField numberOfIterationsTextField;

	@FXML
	private ComboBox<String> classifiersComboBox;

	@FXML
	private ComboBox<Integer> kComboBox;

	@FXML
	private TextArea logTextArea;

	public static String class1Name;
	public static String class2Name;

	private File loadedFile;
	private int numberOfFeatures;

	private List<Integer> trainingIndexes;
	private List<Integer> testIndexes;

	private List<Sample> allSamples;
	private List<Sample> samples1;
	private List<Sample> samples2;
	private List<Sample> trainingSamples;

	private ComputingResult computingResult;

	private Random random;

	@FXML
	public void initialize() {
		initRadioButtons();
		initComboBoxes();
		allSamples = new ArrayList<>();
		samples1 = new ArrayList<>();
		samples2 = new ArrayList<>();
		trainingSamples = new ArrayList<>();
		trainingIndexes = new ArrayList<>();
		testIndexes = new ArrayList<>();

		loadedFile = new File("./src/main/resources/Maple_Oak.txt");
		loadData();

		random = new Random();
	}

	private void initRadioButtons() {
		computingToggleGroup = new ToggleGroup();
		fisherRadioButton.setToggleGroup(computingToggleGroup);
		sfsRadioButton.setToggleGroup(computingToggleGroup);

		validationToggleGroup = new ToggleGroup();
		crossValidationRadioButton.setToggleGroup(validationToggleGroup);
		bootstrapRadioButton.setToggleGroup(validationToggleGroup);
	}

	private void initComboBoxes() {
		initFeaturesComboBox();
		initClassifiersComboBox();
		initKComboBox();
	}

	private void initFeaturesComboBox() {
		ObservableList<Integer> choices = FXCollections.observableArrayList();
		for (int i = 1; i <= 64; i++) {
			choices.add(i);
		}
		numberOfFeaturesComboBox.setItems(choices);
		numberOfFeaturesComboBox.setValue(1);
	}

	private void initClassifiersComboBox() {
		ObservableList<String> choices = FXCollections.observableArrayList();
		choices.add(CalculationMethod.NN.getDisplayName());
		choices.add(CalculationMethod.kNN.getDisplayName());
		choices.add(CalculationMethod.NM.getDisplayName());
		choices.add(CalculationMethod.kNM.getDisplayName());
		classifiersComboBox.setItems(choices);
	}

	private void initKComboBox() {
		ObservableList<Integer> choices = FXCollections.observableArrayList();
		choices.add(1);
		choices.add(3);
		choices.add(5);
		choices.add(7);
		choices.add(9);
		choices.add(11);
		choices.add(13);
		choices.add(15);
		kComboBox.setItems(choices);
	}

	@FXML
	private void loadFileButtonHandler() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Select data file");
		fileChooser.setInitialDirectory(new File("./src/main/resources/"));
		loadedFile = fileChooser.showOpenDialog(mainAnchorPane.getScene().getWindow());
		if (loadedFile != null) {
			loadData();
		}
	}

	@FXML
	private void trainButtonHandler() {
		clearPrevTraining();
		String trainingPartText = trainingPartTextField.getText();
		if (trainingPartText != null && !trainingPartText.isEmpty()) {
			int trainingPartSize = Integer.parseInt(trainingPartText);
			if (isTrainingPartOK(trainingPartSize)) {
				int numberOfAllSamples = allSamples.size();
				while (trainingIndexes.size() < trainingPartSize) {
					int nextRandom = random.nextInt(numberOfAllSamples);
					if (!trainingIndexes.contains(nextRandom)) {
						trainingIndexes.add(nextRandom);
					}
				}
				for (int i = 0; i < numberOfAllSamples; i++) {
					if (!trainingIndexes.contains(i)) {
						testIndexes.add(i);
					}
				}
				log("training part: " + trainingIndexes.toString());
				log("test part: " + testIndexes.toString());
				collectTrainingSamples();
			} else {
				log("Wrong training part. Provide number in range 1-" + (allSamples.size() - 1));
			}
		} else {
			log("!!! Provide Training Part !!!");
		}
	}

	private void collectTrainingSamples() {
		for (int i : trainingIndexes) {
			trainingSamples.add(allSamples.get(i));
		}
	}

	private void clearPrevTraining() {
		trainingIndexes.clear();
		testIndexes.clear();
	}

	private boolean isTrainingPartOK(int trainingPart) {
		return trainingPart < allSamples.size() && trainingPart >= 1;
	}

	@FXML
	private void executeButtonHandler() {
		Object selectedClassifier = classifiersComboBox.getValue();
		if (computingResult == null) {
			log("!!! compute best features first !!!");
		} else if (trainingIndexes.isEmpty()) {
			log("!!! Train First !!!");
		} else if (selectedClassifier == null) {
			log("!!! Choose Classifier !!!");
		} else {
			try {
				String classifier = selectedClassifier.toString();
				if (classifier.equals(CalculationMethod.NN.getDisplayName())) {
					log(nn().toString());
				} else if (classifier.equals(CalculationMethod.kNN.getDisplayName())) {
					log(knn().toString());
				} else if (classifier.equals(CalculationMethod.NM.getDisplayName())) {
					log(nm().toString());
				} else {
					log(knm().toString());
				}
			} catch (Exception e) {
				log(e.getMessage());
			}
		}
	}

	private CalculationResult nn() {
		int ok = 0;
		int ko = 0;
		for (Integer testIndex : testIndexes) {
			Sample testSample = allSamples.get(testIndex);
			Sample closestSample = SMPDMath.findTheClosestSampleByEuclid(allSamples, testSample, trainingIndexes, computingResult);
			if (testSample.getName().equals(closestSample.getName())) {
				ok++;
			} else {
				ko++;
			}
		}
		return new CalculationResult(CalculationMethod.NN, ok, ko);
	}

	private CalculationResult knn() throws Exception {
		Object kString = kComboBox.getValue();
		if (kString == null) {
			throw new Exception("!!! Choose k !!!");
		} else {
			int k = Integer.parseInt(kString.toString());
			int ok = 0;
			int ko = 0;
			for (Integer testIndex : testIndexes) {
				Sample testSample = allSamples.get(testIndex);
				List<Sample> closestSamples = SMPDMath.findKClosestSamplesByEuclid(k, allSamples, testSample, trainingIndexes, computingResult);
				int class1Votes = 0;
				for (Sample sample : closestSamples) {
					if (sample.getName().equals(class1Name)) {
						class1Votes++;
					} else {
						class1Votes--;
					}
				}
				String winnerClass = getWinnerClass(class1Votes);
				if (testSample.getName().equals(winnerClass)) {
					ok++;
				} else {
					ko++;
				}
			}
			return new CalculationResult(CalculationMethod.kNN, ok, ko);
		}
	}

	private String getWinnerClass(int class1Votes) {
		if (class1Votes > 0) {
			return class1Name;
		} else {
			return class2Name;
		}
	}

	private CalculationResult nm() {
		List<Integer> bestFeatures = computingResult.getBestFeatures();
		Map<String, List<Avg>> averages = SMPDMath.calculateAvg(trainingSamples, bestFeatures);
		List<Avg> avgs1 = averages.get(class1Name);
		List<Avg> avgs2 = averages.get(class2Name);
		int ok = 0;
		int ko = 0;
		for (Integer testIndex : testIndexes) {
			Sample testSample = allSamples.get(testIndex);
			double class1Euclid = SMPDMath.euclid(bestFeatures, testSample, avgs1);
			double class2Euclid = SMPDMath.euclid(bestFeatures, testSample, avgs2);
			String winnerClass = getWinnerClass(class1Euclid, class2Euclid);
			if (testSample.getName().equals(winnerClass)) {
				ok++;
			} else {
				ko++;
			}
		}
		return new CalculationResult(CalculationMethod.NM, ok, ko);
	}

	private String getWinnerClass(double class1Euclid, double class2Euclid) {
		String winnerClass;
		if (class1Euclid < class2Euclid) {
			winnerClass = class1Name;
		} else {
			winnerClass = class2Name;
		}
		return winnerClass;
	}

	private CalculationResult knm() throws Exception {
		Object kString = kComboBox.getValue();
		if (kString == null) {
			throw new Exception("!!! Choose k !!!");
		} else {
			/*
			TODO
			 */
			return new CalculationResult(CalculationMethod.kNM);
		}
	}

	@FXML
	private void validateButtonHandler() {
		RadioButton selectedValidation = (RadioButton) validationToggleGroup.getSelectedToggle();
		if (selectedValidation != null) {
			validate(selectedValidation.getText());
		} else {
			log("!!! Select validation type first !!!");
		}
	}

	private void validate(String validationType) {
		try {
			if ("cross-validation".equals(validationType)) {
				String numberOfSetsText = numberOfSetsTextField.getText();
				if (!numberOfSetsText.isEmpty()) {
					log(crossValidation(Integer.parseInt(numberOfSetsText)).toString());
				} else {
					log("!!! provide number of sets for cross-validation !!!");
				}
			} else if ("bootstrap".equals(validationType)) {
				String numberOfIterationsText = numberOfIterationsTextField.getText();
				if (!numberOfIterationsText.isEmpty()) {
					log(bootstrap(Integer.parseInt(numberOfIterationsText)).toString());
				} else {
					log("!!! provide number of iterations for bootstrap validation !!!");
				}
			}
		} catch (Exception e) {
			log("Something went wrong during validation", e);
		}
	}

	private ValidationResult crossValidation(int numberOfSets) {
		ValidationResult validationResult = new ValidationResult("cross-validation");
		/*
		TODO
		 */
		return validationResult;
	}

	private ValidationResult bootstrap(int numberOfIterations) throws Exception {
		ValidationResult validationResult = new ValidationResult("bootstrap");
		trainForBootstrap();
		validationResult.setNnResult(nn());
		validationResult.setKnnResult(knn());
		validationResult.setNmResult(nm());
		validationResult.setKnmResult(knm());
		for (int i = 1; i < numberOfIterations; i++) {
			trainForBootstrap();
			validationResult.addCalculationResult(nn());
			validationResult.addCalculationResult(knn());
			validationResult.addCalculationResult(nm());
			validationResult.addCalculationResult(knm());
		}
		return validationResult;
	}

	private void trainForBootstrap() {
		clearPrevTraining();
		int numberOfSamples = allSamples.size();

		for (int sampleIndex = 0; sampleIndex < numberOfSamples; sampleIndex++) {
			int randomIndex = random.nextInt(numberOfSamples);
			if (!trainingIndexes.contains(randomIndex)) {
				trainingIndexes.add(randomIndex);
			}
		}

		for (int sampleIndex = 0; sampleIndex < numberOfSamples; sampleIndex++) {
			if (!trainingIndexes.contains(sampleIndex)) {
				testIndexes.add(sampleIndex);
			}
		}

		collectTrainingSamples();
	}

	private void loadData() {
		try {
			samples1.clear();
			samples2.clear();
			allSamples.clear();
			Scanner scanner = new Scanner(loadedFile);
			String firstLine = scanner.nextLine();
			numberOfFeatures = Integer.parseInt(firstLine.substring(0, firstLine.indexOf(",")));
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String className = line.substring(0, line.indexOf(LINE_PARAM_SEPARATOR));
				if (className.contains(" ")) {
					className = className.substring(0, className.indexOf(" "));
				}
				if (class1Name == null) {
					class1Name = className;
				} else if (!className.equals(class1Name)) {
					class2Name = className;
				}
				String[] features = line.substring(line.indexOf(LINE_PARAM_SEPARATOR) + 1, line.length()).split(LINE_PARAM_SEPARATOR);
				if (className.equals(class1Name)) {
					samples1.add(new Sample(className, features));
				} else {
					samples2.add(new Sample(className, features));
				}
			}
			allSamples.addAll(samples1);
			allSamples.addAll(samples2);

			updateLabels();
			log("File " + loadedFile.getName() + " loaded successfully", null);
		} catch (FileNotFoundException e) {
			log(e.getMessage(), e);
		}
	}

	private void updateLabels() {
		classesLabel.setText(String.valueOf(2));
		samplesLabel.setText(String.valueOf(allSamples.size()));
		featuresLabel.setText(String.valueOf(numberOfFeatures));
	}

	@FXML
	private void computeButtonHandler() {
		RadioButton selectedRadio = (RadioButton) computingToggleGroup.getSelectedToggle();
		try {
			computingResult = calculate(selectedRadio);
			if (computingResult != null) {
				log(selectedRadio.getText() + " value: " + computingResult.getValue() + " - best features: " + computingResult.getBestFeatures().toString(), null);
			} else {
				throw new Exception("Can't calculate. Something went wrong.", null);
			}
		} catch (Exception e) {
			log(e.getMessage(), e);
		}
	}

	private ComputingResult calculate(RadioButton selectedRadio) throws Exception {
		String selectedRadioText = selectedRadio.getText();
		Integer selectedNumberOfFeatures = numberOfFeaturesComboBox.getValue();
		if ("Fisher".equals(selectedRadioText)) {
			return fisher(selectedNumberOfFeatures);
		} else if ("SFS".equals(selectedRadioText)) {
			return sfs(selectedNumberOfFeatures);
		} else {
			throw new Exception("There is no calculation implemented for option " + selectedRadioText);
		}
	}

	private ComputingResult fisher(Integer selectedNumberOfFeatures) {
		if (selectedNumberOfFeatures == 1) {
			return fisherForOne();
		} else {
			return fisherForMany(selectedNumberOfFeatures);
		}
	}

	private ComputingResult fisher(List<Integer> bestSamples) {
		return fisherForMany(bestSamples);
	}

	private ComputingResult fisherForOne() {
		ComputingResult result = null;
		for (int featureIndex = 0; featureIndex < numberOfFeatures; featureIndex++) {
			Map<String, Avg> averages = SMPDMath.calculateAvg(allSamples, featureIndex);
			Avg avg1 = averages.get(class1Name);
			Avg avg2 = averages.get(class2Name);

			Map<String, Double> standardDeviations = SMPDMath.calculateStandardDeviation(class1Name, class2Name, allSamples, featureIndex, avg1, avg2);
			double standardDev1 = standardDeviations.get(class1Name);
			double standardDev2 = standardDeviations.get(class2Name);

			double fisherFraction = Math.abs(avg1.getValue() - avg2.getValue());
			double fisherDivision = standardDev1 + standardDev2;
			double fisherValue = fisherFraction / fisherDivision;
			if (result == null || fisherValue > result.getValue()) {
				result = new ComputingResult(Collections.singletonList(featureIndex), fisherValue);
			}
		}
		return result;
	}

	private ComputingResult fisherForMany(Integer selectedNumberOfFeatures) {
		List<List<Integer>> allIndexes = collectIndexes(selectedNumberOfFeatures);
		return calculateFisher(allIndexes);
	}

	private ComputingResult fisherForMany(List<Integer> bestSamples) {
		List<List<Integer>> indexes = collectIndexes(bestSamples);
		return calculateFisher(indexes);
	}

	private ComputingResult calculateFisher(List<List<Integer>> allIndexes) {
		ComputingResult result = null;
		for (List<Integer> indexes : allIndexes) {
			Map<String, List<Avg>> averages = SMPDMath.calculateAvg(allSamples, indexes);
			List<Avg> avgs1 = averages.get(class1Name);
			List<Avg> avgs2 = averages.get(class2Name);

			Map<String, DoubleMatrix2D> covariances = SMPDMath.covarianceMatrix(class1Name, class2Name, samples1, samples2, indexes, avgs1, avgs2);
			DoubleMatrix2D c1Matrix = covariances.get(class1Name);
			DoubleMatrix2D c2Matrix = covariances.get(class2Name);

			double fisherFraction = SMPDMath.euclid(avgs1, avgs2);
			double fisherDivision = SMPDMath.ALGEBRA.det(c1Matrix) + SMPDMath.ALGEBRA.det(c2Matrix);
			double fisherValue = fisherFraction / fisherDivision;
			if (result == null || fisherValue > result.getValue()) {
				result = new ComputingResult(indexes, fisherValue);
			}
		}
		return result;
	}

	private List<List<Integer>> collectIndexes(Integer selectedNumberOfFeatures) {
		List<Integer> indexes = new ArrayList<>();
		for (int i = 0; i < numberOfFeatures; i++) {
			indexes.add(i);
		}
		return SMPDMath.collectCombinations(indexes, selectedNumberOfFeatures, 0, new Integer[selectedNumberOfFeatures], new ArrayList<>());
	}

	private List<List<Integer>> collectIndexes(List<Integer> alreadySelected) {
		List<List<Integer>> combinations = new ArrayList<>();
		for (int i = 0; i < numberOfFeatures; i++) {
			List<Integer> tempAlreadySelected = new ArrayList<>(alreadySelected);
			if (!tempAlreadySelected.contains(i)) {
				tempAlreadySelected.add(i);
				combinations.add(tempAlreadySelected);
			}
		}
		return combinations;
	}

	private ComputingResult sfs(Integer selectedNumberOfFeatures) {
		ComputingResult result = fisher(1);
		for (int i = 2; i <= selectedNumberOfFeatures; i++) {
			result = fisher(result.getBestFeatures());
		}
		return result;
	}

	private void log(String text) {
		log(text, null);
	}

	private void log(String text, Exception e) {
		if (e == null) {
			LOGGER.info(text);
		} else {
			LOGGER.error(text, e);
		}
		logTextArea.setText(text + "\n\n" + logTextArea.getText());
	}
}
