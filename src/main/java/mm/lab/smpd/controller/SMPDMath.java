package mm.lab.smpd.controller;

import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mm.lab.smpd.model.Avg;
import mm.lab.smpd.model.ComputingResult;
import mm.lab.smpd.model.EuclidDistance;
import mm.lab.smpd.model.Sample;

public class SMPDMath {

	public static final Algebra ALGEBRA = new Algebra();

	public static Map<String, DoubleMatrix2D> covarianceMatrix(String class1, String class2, List<Sample> samples1, List<Sample> samples2, List<Integer> indexes, List<Avg> avgs1, List<Avg> avgs2) {
		Map<String, DoubleMatrix2D> result = new HashMap<>();

		double[] avgs11 = new double[avgs1.size()];
		double[] avgs22 = new double[avgs2.size()];
		for (int i = 0; i < avgs1.size(); i++) {
			avgs11[i] = avgs1.get(i).getValue();
			avgs22[i] = avgs2.get(i).getValue();
		}

		DoubleMatrix2D temp1Matrix = createTempMatrix(samples1, indexes, avgs11);
		DoubleMatrix2D temp2Matrix = createTempMatrix(samples2, indexes, avgs22);
		DoubleMatrix2D temp1TransposeMatrix = ALGEBRA.transpose(temp1Matrix);
		DoubleMatrix2D temp2TransposeMatrix = ALGEBRA.transpose(temp2Matrix);
		DoubleMatrix2D cov1 = ALGEBRA.mult(temp1Matrix, temp1TransposeMatrix);
		DoubleMatrix2D cov2 = ALGEBRA.mult(temp2Matrix, temp2TransposeMatrix);
		cov1 = divideValues(cov1, samples1.size());
		cov2 = divideValues(cov2, samples2.size());

		result.put(class1, cov1);
		result.put(class2, cov2);
		return result;
	}

	private static DoubleMatrix2D divideValues(DoubleMatrix2D covarianceMatrix, int samplesSize) {
		for (int i = 0; i < covarianceMatrix.rows(); i++) {
			for (int j = 0; j < covarianceMatrix.columns(); j++) {
				covarianceMatrix.set(i, j, covarianceMatrix.get(i, j) / samplesSize);
			}
		}
		return covarianceMatrix;
	}

	private static DoubleMatrix2D createTempMatrix(List<Sample> samples, List<Integer> indexes, double[] avgs) {
		double[][] tempMatrix = new double[indexes.size()][samples.size()];
		for (int i = 0; i < samples.size(); i++) {
			for (int j = 0; j < indexes.size(); j++) {
				double featureValue = samples.get(i).getFeatures().get(indexes.get(j));
				tempMatrix[j][i] = featureValue - avgs[j];
			}
		}
		return DoubleFactory2D.dense.make(tempMatrix);
	}

	public static double euclid(List<Avg> avgs1, List<Avg> avgs2) {
		List<Double> avgDiv = new ArrayList<>();
		for (int i = 0; i < avgs1.size(); i++) {
			avgDiv.add(avgs1.get(i).getValue() - avgs2.get(i).getValue());
		}
		double powerSum = 0.0;
		for (double avg : avgDiv) {
			powerSum += Math.pow(avg, 2);
		}
		return Math.sqrt(powerSum);
	}

	public static double euclid(List<Integer> bestFeatures, Sample testSample, List<Avg> avgs) {
		List<Double> testSampleFeatures = testSample.getFeatures();
		double distance = 0;
		for (int i = 0; i < bestFeatures.size(); i++) {
			distance += Math.pow(testSampleFeatures.get(bestFeatures.get(i)) - avgs.get(i).getValue(), 2);
		}
		distance = Math.sqrt(distance);

		return distance;
	}

	public static Sample findTheClosestSampleByEuclid(List<Sample> allSamples, Sample testSample, List<Integer> trainingIndexes, ComputingResult computingResult) {
		List<Double> testSampleFeatures = testSample.getFeatures();
		double currentDistance;
		double bestDistance = 9999999999.0;
		int bestIndex = -1;
		for (int i = 0; i < trainingIndexes.size(); i++) {
			List<Double> trainingSampleFeatures = allSamples.get(trainingIndexes.get(i)).getFeatures();
			currentDistance = 0;
			for (int featureIndex : computingResult.getBestFeatures()) {
				currentDistance += Math.pow(testSampleFeatures.get(featureIndex) - trainingSampleFeatures.get(featureIndex), 2);
			}
			currentDistance = Math.sqrt(currentDistance);
			if (currentDistance < bestDistance) {
				bestDistance = currentDistance;
				bestIndex = i;
			}
		}
		return allSamples.get(trainingIndexes.get(bestIndex));
	}

	public static List<Sample> findKClosestSamplesByEuclid(int k, List<Sample> allSamples, Sample testSample, List<Integer> trainingIndexes, ComputingResult computingResult) {
		List<Double> testSampleFeatures = testSample.getFeatures();
		double currentDistance;
		List<EuclidDistance> distances = new ArrayList<>();
		for (int i = 0; i < trainingIndexes.size(); i++) {
			List<Double> trainingSampleFeatures = allSamples.get(trainingIndexes.get(i)).getFeatures();
			currentDistance = 0;
			for (int featureIndex : computingResult.getBestFeatures()) {
				currentDistance += Math.pow(testSampleFeatures.get(featureIndex) - trainingSampleFeatures.get(featureIndex), 2);
			}
			distances.add(new EuclidDistance(i, Math.sqrt(currentDistance)));
		}
		Collections.sort(distances);
		List<Sample> closestSamples = new ArrayList<>();
		for (int i = 0; i < k; i++) {
			closestSamples.add(allSamples.get(trainingIndexes.get(distances.get(i).getIndex())));
		}
		return closestSamples;
	}

	public static Map<String, Double> calculateStandardDeviation(String class1, String class2, List<Sample> samples, int featureIndex, Avg avg1, Avg avg2) {
		Map<String, Double> standardDeviations = new HashMap<>();
		standardDeviations.put(class1, 0.0);
		standardDeviations.put(class2, 0.0);
		for (Sample sample : samples) {
			String sampleName = sample.getName();
			double featureValue = sample.getFeatures().get(featureIndex);
			Double tempStandardDevValue = standardDeviations.get(sampleName);
			tempStandardDevValue += Math.pow(featureValue, 2);
			standardDeviations.put(sampleName, tempStandardDevValue);
		}
		standardDeviations.put(class1, Math.sqrt(standardDeviations.get(class1) / avg1.getCounter() - (Math.pow(avg1.getValue(), 2))));
		standardDeviations.put(class2, Math.sqrt(standardDeviations.get(class2) / avg2.getCounter() - (Math.pow(avg2.getValue(), 2))));
		return standardDeviations;
	}

	public static Map<String, Avg> calculateAvg(List<Sample> samples, int featureIndex) {
		Map<String, Avg> averages = new HashMap<>();
		for (Sample sample : samples) {
			String sampleName = sample.getName();
			Avg avg = new Avg();
			if (averages.containsKey(sampleName)) {
				avg = averages.get(sampleName);
			}
			avg.addParam(sample.getFeatures().get(featureIndex));
			averages.put(sampleName, avg);
		}
		return averages;
	}

	public static Map<String, List<Avg>> calculateAvg(List<Sample> samples, List<Integer> featureIndexes) {
		Map<String, List<Avg>> averages = new HashMap<>();
		for (int featureIndex : featureIndexes) {
			Map<String, Avg> avg = calculateAvg(samples, featureIndex);
			if (averages.isEmpty()) {
				for (String key : avg.keySet()) {
					averages.put(key, new ArrayList<>());
				}
			}
			for (String key : avg.keySet()) {
				averages.get(key).add(avg.get(key));
			}
		}
		return averages;
	}

	public static List<List<Integer>> collectCombinations(List<Integer> indexes, int selectedNumberOfFeatures, int startPosition, Integer[] singleCombination, List<List<Integer>> result) {
		if (selectedNumberOfFeatures == 0) {
			result.add(new ArrayList<>(Arrays.asList(singleCombination)));
		} else {
			for (int i = startPosition; i <= indexes.size() - selectedNumberOfFeatures; i++) {
				singleCombination[singleCombination.length - selectedNumberOfFeatures] = i;
				result = collectCombinations(indexes, selectedNumberOfFeatures - 1, i + 1, singleCombination, result);
			}
		}
		return result;
	}
}
