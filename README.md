# SMPDLab
##### Project created for university purpose

1. Rozbudowanie szablonu programu o opcję wyboru dowolnej liczby cech (n, gdzie n
może przyjąć dowolną wartość z zakresu 1 - 64) dla rozważanych 2 klas obiektów
(Acer i Quercus). Należy sprawdzić wszystkie możliwe kombinacje n cech i wybrać
najlepszą z nich. Wybrane cechy mogą zostać wypisane w oknie programu lub
w konsoli.

2. Zaimplementowanie algorytmu SFS (Sequential Forward Selection) w celu
przyspieszenia wyboru cech. Porównanie wyników działania obu metod. Program ma
umożliwić wybór dowolnej liczby cech (n, gdzie n może przyjąć dowolną wartość z
zakresu 1 - 64) dla rozważanych 2 klas obiektów (Acer i Quercus).

3. Zaimplementowanie klasyfikatorów NN, k-NN, NM i k-NM. W celu sprawdzenia
jakości działania klasyfikatorów zbiór wejściowy należy podzielić na część
treningową i testową. Podział na zbiór testowy i treningowy ma nastąpić po
naciśnięciu przycisku Train, wartość wpisana w polu Training part oznacza jaka
część próbek ma należeć do zbioru treningowego. Po naciśnięciu przycisku Execute
program ma wyświetlić skuteczność działania wybranego klasyfikatora (ile procent
próbek testowych zostało poprawnie zaklasyfikowanych). Program musi umożliwić
sprawdzenie wszystkich klasyfikatorów dla tego samego zbioru danych testowych i
treningowych.

4. Zaimplementowanie metod bootstrap i kroswalidacji do oceny jakości klasyfikacji.
Program musi umożliwić sprawdzenie wszystkich klasyfikatorów dla tego samego
zbioru danych testowych i treningowych. Program ma wyświetlić skuteczność
działania wybranego klasyfikatora (ile procent próbek testowych zostało poprawnie
zaklasyfikowanych). Program musi umożliwić wprowadzenie liczby przedziałów
(kroswalidacja) i liczby iteracji (bootstrap).

5. (dla osób powtarzających przedmiot)
Dodanie modułu przetwarzania obrazów, który będzie wczytywał obraz czarno biały,
następnie wykrywał krawędzie i wyznaczał deskryptory Fouriera dla nich. Wynik
działania algorytmu ma być plikiem wejściowym dla zadań 1 - 4.

Warunki zaliczenia:
Warunkiem koniecznym zaliczenia przedmiotu jest wykonanie zadania 1, 2,
zaimplementowanie trzech wybranych klasyfikatorów z zadania 3 oraz jednej metody
z zadania 4. Osoby powtarzające przedmiot dodatkowo są zobowiązane do
wykonanie zadania 5.
